Adminify
===
Simplify the Drupal admin.

You need adminify if:

* You want to create different administration for your users.
* You want an admin login form
* You want different admin theme for different user roles.
